package com.epam.tasks;

import com.epam.tasks.service.AbiturientService;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        AbiturientService abiturientService = new AbiturientService();
        System.out.println("Список абитуриентов: ");
        System.out.println(Arrays.toString(abiturientService.getAbiturientRepo()));
        System.out.println("Список зачисленных: ");
        System.out.println(Arrays.toString(abiturientService.getStudent()));
    }
}
