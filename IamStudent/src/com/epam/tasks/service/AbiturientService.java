package com.epam.tasks.service;

import com.epam.tasks.model.Abiturient;
import com.epam.tasks.repo.AbiturientRepo;

import java.util.Comparator;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class AbiturientService implements AbiturInterface {
    private AbiturientRepo abiturientRepo = new AbiturientRepo();

    public Abiturient[] getStudent() {
        Stream<Abiturient> abiturientStream = Stream.of(abiturientRepo.getAbiturients());
        return abiturientStream
                .sorted(new AbiturComparator().reversed())
                .limit(5)
                .toArray(Abiturient[]::new);
    }

    public Abiturient[] getAbiturientRepo() {
        return abiturientRepo.getAbiturients();
    }

    class AbiturComparator implements Comparator<Abiturient> {

        @Override
        public int compare(Abiturient abiturient1, Abiturient abiturient2) {
            return IntStream.of(abiturient1.getScore()).average().toString().compareTo(IntStream.of(abiturient2.getScore()).average().toString());
        }
    }
}
