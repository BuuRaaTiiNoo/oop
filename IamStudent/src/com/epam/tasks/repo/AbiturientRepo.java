package com.epam.tasks.repo;

import com.epam.tasks.model.Abiturient;

public class AbiturientRepo {
    private Abiturient[] abiturients = new Abiturient[]{
            new Abiturient("Вася", "Иванов", new int[]{3, 3, 3, 3, 4}),
            new Abiturient("Петя", "Петров", new int[]{3, 3, 3, 3, 3}),
            new Abiturient("Коля", "Сидоров", new int[]{3, 3, 4, 4, 4}),
            new Abiturient("Ваня", "Смирнов", new int[]{3, 3, 4, 4, 5}),
            new Abiturient("Саша", "Богданов", new int[]{3, 3, 4, 5, 5}),
            new Abiturient("Маша", "Богданова", new int[]{3, 4, 4, 4, 5}),
            new Abiturient("Лена", "Смирнова", new int[]{4, 4, 4, 5, 5}),
            new Abiturient("Оля", "Сидорова", new int[]{4, 4, 5, 5, 5}),
            new Abiturient("Катя", "Петрова", new int[]{4, 5, 5, 5, 5}),
            new Abiturient("Настя", "Иванова", new int[]{5, 5, 5, 5, 5})
    };

    public Abiturient[] getAbiturients() {
        return abiturients;
    }
}
