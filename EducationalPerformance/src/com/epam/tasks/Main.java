package com.epam.tasks;

import com.epam.tasks.service.groupService.GroupServiceImpl;

import java.util.Arrays;

public class Main {

    public static void main(String[] args) {
        GroupServiceImpl groupStudent = new GroupServiceImpl();
        System.out.println(groupStudent.getGroupStudent().getStudents().toString());
        System.out.println("Средний балл группы: " + groupStudent.middleScoreGroup());
        System.out.println("Колличество отличников: " + new GroupServiceImpl().countAchiever());
        System.out.println("Колличество неуспевающих: " + new GroupServiceImpl().countDowager());
    }
}
