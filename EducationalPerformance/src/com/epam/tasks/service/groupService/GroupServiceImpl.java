package com.epam.tasks.service.groupService;

import com.epam.tasks.model.group.GroupStudent;
import com.epam.tasks.model.student.Student;

import java.util.Arrays;
import java.util.stream.IntStream;

public class GroupServiceImpl implements GroupService {
    private GroupStudent groupStudent = new GroupStudent();

    public double middleScoreGroup() {
        double average = 0;
        for (Student student : groupStudent.getStudents()) {
            average = IntStream.of(student.getScore()).average().getAsDouble();
        }
        return average;
    }

    public int countAchiever() {
        int count = 0;
        for (Student student : groupStudent.getStudents()) {
            if ((int) IntStream.of(student.getScore()).average().getAsDouble() == 5)
                count++;
        }
        return count;
    }

    public int countDowager() {
        int count = 0;
        for (Student student : groupStudent.getStudents()) {
            if (Arrays.stream(student.getScore()).min().getAsInt() < 3)
                count++;
        }
        return count;
    }

    public GroupStudent getGroupStudent() {
        return groupStudent;
    }
}
