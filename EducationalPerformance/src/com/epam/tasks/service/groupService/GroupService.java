package com.epam.tasks.service.groupService;

public interface GroupService {
    public double middleScoreGroup();

    public int countAchiever();

    public int countDowager();
}
