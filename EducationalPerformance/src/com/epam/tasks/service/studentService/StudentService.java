package com.epam.tasks.service.studentService;

import com.epam.tasks.model.student.Student;

public interface StudentService {
    public double middleScore(Student student);
}
