package com.epam.tasks.service.studentService;

import com.epam.tasks.model.student.Student;

import java.util.stream.IntStream;

public class StudentServiceImpl implements StudentService {
    public double middleScore(Student student){
        return IntStream.of(student.getScore()).average().getAsDouble();
    }
}
