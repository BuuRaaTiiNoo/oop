package com.epam.tasks.model.student;

import com.epam.tasks.service.studentService.StudentServiceImpl;

public class Student {
    private String firstName;
    private String secondName;
    private int[] score;

    public Student(String firstName, String secondName, int[] score) {
        this.firstName = firstName;
        this.secondName = secondName;
        this.score = score;
    }

    public Student() {
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public int[] getScore() {
        return score;
    }

    public void setScore(int[] score) {
        this.score = score;
    }

    @Override
    public String toString() {
        return firstName + " " + secondName + " " + new StudentServiceImpl().middleScore(this);
    }
}
