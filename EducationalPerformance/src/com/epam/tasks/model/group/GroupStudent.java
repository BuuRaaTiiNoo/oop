package com.epam.tasks.model.group;

import com.epam.tasks.model.student.Student;

import java.util.ArrayList;
import java.util.Arrays;

public class GroupStudent {
    private ArrayList<Student> students = new ArrayList<>(Arrays.asList(
            new Student("Вася", "Иванов", new int[]{5, 5, 5, 5, 5}),
            new Student("Петя", "Петров", new int[]{5, 5, 5, 5, 5}),
            new Student("Ваня", "Сидоров", new int[]{2, 1, 3, 4, 5}),
            new Student("Гриша", "Сидоров", new int[]{3, 3, 2, 4, 5}),
            new Student("Коля", "Сидоров", new int[]{4, 4, 5, 4, 5})
    ));

    public ArrayList<Student> getStudents() {
        return students;
    }
}
