package com.epam.tasks.Library;

import com.epam.tasks.model.Book;

import java.util.ArrayList;
import java.util.Arrays;

public class Library {
    private ArrayList<Book> bookArrayList = new ArrayList<>(Arrays.asList(
            new Book(0,"И грянул гром","Рей Брэдбери"),
            new Book(1,"Пятая четверть","Стивен Кинг"),
            new Book(2,"Ур","Стивен Кинг"),
            new Book(3,"Маленький принц","Антуан де Сент-Экзюпери")
    ));

    public ArrayList<Book> getBookArrayList() {
        return bookArrayList;
    }
}
