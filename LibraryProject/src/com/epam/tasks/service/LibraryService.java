package com.epam.tasks.service;
import com.epam.tasks.model.Book;
import java.util.ArrayList;

public interface LibraryService {

    public void addBook(Book book);

    public void delBook(int id);

    public Book getBookByName(String nameBook);

    public Book getBookByAuthor(String authorBook);

    public ArrayList<Book> getBookArrayList();
}
