package com.epam.tasks.service;

import com.epam.tasks.Library.Library;
import com.epam.tasks.model.Book;

import java.util.ArrayList;

public class LibraryServiceImpl implements LibraryService {
    private Library library = new Library();

    public void addBook(Book book) {
        library.getBookArrayList().add(book);
    }

    public void delBook(int id) {
        library.getBookArrayList().remove(id);
    }

    public void delBook(String nameBook) {
        for (Book book : library.getBookArrayList()) {
            if (book.getBookName().equals(nameBook))
                library.getBookArrayList().remove(book);
        }
    }

    public Book getBookByName(String nameBook) {
        for (Book aBookArrayList : library.getBookArrayList()) {
            if (aBookArrayList.getBookName().equals(nameBook))
                return aBookArrayList;
        }
        return null;
    }

    public Book getBookByAuthor(String authorBook) {

        for (Book aBookArrayList : library.getBookArrayList()) {
            if (aBookArrayList.getAuthorBook().equals(authorBook))
                return aBookArrayList;
        }
        return null;
    }

    public ArrayList<Book> getBookArrayList() {
        return library.getBookArrayList();
    }
}
