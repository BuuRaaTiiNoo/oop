package com.epam.tasks;

import com.epam.tasks.model.Book;
import com.epam.tasks.service.LibraryServiceImpl;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        LibraryServiceImpl libraryService = new LibraryServiceImpl();
        Boolean isExit = true;
        Book book;
        Scanner scanner = new Scanner(System.in);
        System.out.println("Список команд: \naddBook - добавить книгу" +
                "\ngetBooks - получить список книг в библиотеке" +
                "\ndelBookById - удалить книгу по id" +
                "\ndelBookById - удалить книгу по названию" +
                "\nsearchBookByName - поиск книги по названию" +
                "\nsearchBookByAuthor - поиск книги по автору" +
                "\nexit - выход");
        while (isExit) {
            switch (scanner.nextLine()) {
                case "exit":
                    isExit = false;
                    break;
                case "getBooks":
                    System.out.println("Список книг в библиотеке:");
                    for (int i = 0; i < libraryService.getBookArrayList().size(); i++) {
                        System.out.println(libraryService.getBookArrayList().get(i).toString());
                    }
                    break;
                case "addBook":
                    book = new Book();
                    book.setId(libraryService.getBookArrayList().size());
                    System.out.print("Название книги: ");
                    book.setBookName(scanner.nextLine());
                    System.out.print("Автор книги: ");
                    book.setAuthorBook(scanner.nextLine());
                    libraryService.addBook(book);
                    break;
                case "delBookById":
                    System.out.print("Id книги для удаления: ");
                    libraryService.delBook(scanner.nextInt());
                    break;
                case "delBookByName":
                    System.out.print("Название книги для удаления: ");
                    libraryService.delBook(scanner.nextLine());
                    break;
                case "searchBookByName":
                    System.out.println("Название книги: ");
                    System.out.println(libraryService.getBookByName(scanner.nextLine()));
                    break;
                case "searchBookByAuthor":
                    System.out.println("Имя автора: ");
                    System.out.println(libraryService.getBookByAuthor(scanner.nextLine()));
                    break;
            }
        }
    }
}
