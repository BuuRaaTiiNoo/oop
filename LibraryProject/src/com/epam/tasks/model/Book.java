package com.epam.tasks.model;

public class Book {
    private int id;
    private String bookName;
    private String authorBook;

    public Book() {
    }

    public Book(int id, String bookName, String authorBook) {
        this.id = id;
        this.bookName = bookName;
        this.authorBook = authorBook;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBookName() {
        return bookName;
    }

    public void setBookName(String bookName) {
        this.bookName = bookName;
    }

    public String getAuthorBook() {
        return authorBook;
    }

    public void setAuthorBook(String authorBook) {
        this.authorBook = authorBook;
    }

    @Override
    public String toString() {
        return id + ". " + bookName + ". " + authorBook;
    }
}
